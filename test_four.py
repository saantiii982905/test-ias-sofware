import json
from collections import defaultdict


class Products(object):
    id = None
    name = None
    barcode = None

    def __init__(self, pk, name, barcode):
        self.id = pk
        self.name = name
        self.barcode = barcode
    
    def __str__(self):
        return f'Producto {self.id}'


list_products = [
    {
        "name": "Zapatos XYZ",
        "bardcode": "8569741233658",
        "marker": "Deportes XYZ",
        "category": "Zapatos",
        "gender": "Masculino"
    },
    {
        "name": "Zapatos ABC",
        "bardcode": "7452136985471",
        "marker": "Deportes XYZ",
        "category": "Zapatos",
        "gender": "Femenino"
    },
    {
        "name": "Camisa DEF",
        "bardcode": "5236412896324",
        "marker": "Deportes XYZ",
        "category": "Camisas",
        "gender": "Masculino"
    },
    {
        "name": "Bolso KLM",
        "bardcode": "5863219635478",
        "marker": "Carteras Hi-Fashion",
        "category": "Bolsos",
        "gender": "Femenino"
    },    
]

def add_dict(key, dic):
    if not key in dic:
        return True
    else:
        return False



def group_by(list_products):
    context = defaultdict(dict)
    aux = 0
    for products in list_products:
        aux += 1
        
        instance_prod = Products(aux, products['name'], products['bardcode'])
        categories = defaultdict(dict)
        gender = defaultdict(list)
        gender[products['gender']].append(instance_prod.__str__())
        categories[products['category']] = gender
        if context[products['marker']]:
            categories_aux = products['category']
            dic = context[products['marker']][categories_aux]
            if add_dict(categories_aux, dic):
                gender_aux = products['gender']
                print("gender ux: ", gender_aux, " dic: ", dic.keys())
                if add_dict(gender_aux, dic):
                    print("Entra")
                    if len(dic.keys()) == 0:
                        context[products['marker']][categories_aux] = gender
                    else:
                        context[products['marker']][categories_aux][gender_aux].append(instance_prod.__str__())
                    
                else:
                    context[products['marker']][categories_aux][gender_aux].append(instance_prod.__str__())
                    

        else:
            context[products['marker']] = categories
    return context

data = group_by(list_products)
#print("data: ", data)
print(json.dumps(data, sort_keys=False, indent=4))