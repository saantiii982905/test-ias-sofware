
file = open('file.txt', 'r')
list_words = []

for line in file:
    word = (line.replace("\n", "")).replace(" ", "")
    list_words.append(word)

num_words = int(list_words[0])
list_words.pop(0)
list_words2 = list_words.copy()

aux = 0


def word_equal(word, list_words):
    return len(list(filter(lambda x: word == x.lower(), list_words)))

def update_list(word, list_words):
    while True:
        if word in list_words:
            list_words.remove(word)
        else:
            break
    return list_words

list_dist = []
filtered_word_1 = len(list(dict.fromkeys(list_words)))

for iter in list_words:
    word = iter
    filtered_word_2 = word_equal(word, list_words2)
    list_words2 = update_list(word, list_words2)
    if filtered_word_2 != 0:
        list_dist.append(filtered_word_2)

print("list 1: ", filtered_word_1)
print("list 2: ", list_dist)
    
