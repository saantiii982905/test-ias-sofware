
import math


class Complex(object):
    real_part = None
    imaginary_part = None

    def __init__(self, real_part, imaginary_part):
        self.real_part = float(real_part)
        self.imaginary_part = float(imaginary_part)


    def __add__(self, other_class):
        num_one = self.real_part - other_class.real_part
        num_two = self.imaginary_part - other_class.imaginary_part
        print(f'{num_one} + {num_two}i')
    
    def __sub__(self, other_class):
        num_one = self.real_part - other_class.real_part
        num_two = self.imaginary_part - other_class.imaginary_part
        if num_two < 0:
            print(
                f'{(num_one)} - {abs(num_two)}i'
            )
        else:
            print(
                f'{(num_one)} - {num_two}i'
            )

    def __mul__(self, other_class):
        num_one = (
            (self.real_part * other_class.real_part) - 
            (self.imaginary_part * other_class.imaginary_part)
        )
        num_two = (
            (self.real_part * other_class.imaginary_part) +
            (self.imaginary_part * other_class.real_part)
        )
        print(f'{num_one} + {num_two}i')

    def __truediv__(self, other_class):
        num_one = (
            (
                (self.real_part * other_class.real_part) + (self.imaginary_part * other_class.imaginary_part)
            )/ (
                (other_class.real_part**2 + other_class.imaginary_part**2)
            )
        )
        num_two = (
            (
                (self.imaginary_part * other_class.real_part) - (self.real_part * other_class.imaginary_part)
            ) / (
                (other_class.real_part**2 + other_class.imaginary_part**2)
            )
        )
        print(f'{num_one:.2f} + {num_two:.2f}i')
    

class Mod(object):
    result = None

    def __init__(self, complex_class):
        result = math.sqrt(((complex_class.real_part**2) + (complex_class.imaginary_part**2)))
        print(f'{result:.3} + 0.00i')

A = Complex(2, 1)
B = Complex(5, 6)
A + B
A - B
A * B
A / B
Mod(A)
Mod(B)
