import pandas as pd
from pytz import timezone, all_timezones
from pandas.tseries.holiday import USFederalHolidayCalendar as calendar
from pandas.tseries.holiday import Holiday, AbstractHolidayCalendar, Easter, Day
from pandas.tseries.offsets import CustomBusinessDay
from datetime import datetime, timedelta, tzinfo


cal = calendar()

def strict_next_monday(dt: datetime) -> datetime:
    """
    Si el festivo cae en un día diferente a lunes, se corre al próximo lunes
    """
    if dt.weekday() > 0:
        return dt + timedelta(7-dt.weekday())
    return dt

def all_timezone():
    print('Timezones')
    for timeZone in all_timezones:
        print(timeZone)

#all_timezone()

class ColombianBusinessCalendar(AbstractHolidayCalendar):
    rules = [
        # festivos fijos
        Holiday('Año nuevo', month=1, day=1),
        Holiday('Día del trabajo', month=5, day=1),
        Holiday('Día de la independencia', month=7, day=20),
        Holiday('Batalla de Boyacá', month=8, day=7),
        Holiday('Inmaculada Concepción', month=12, day=8),
        Holiday('Navidad', month=12, day=25),
        # festivos relativos a la pascua
        Holiday('Jueves santo', month=1, day=1, offset=[Easter(), Day(-3)]),
        Holiday('Viernes santo', month=1, day=1, offset=[Easter(), Day(-2)]),
        Holiday('Ascención de Jesús', month=1, day=1, offset=[Easter(), Day(43)]),
        Holiday('Corpus Christi', month=1, day=1, offset=[Easter(), Day(64)]),
        Holiday('Sagrado Corazón de Jesús', month=1, day=1, offset=[Easter(), Day(71)]),
        # festivos desplazables (Emiliani)
        Holiday('Epifanía del señor', month=1, day=6, observance=strict_next_monday),
        Holiday('Día de San José', month=3, day=19, observance=strict_next_monday),
        Holiday('San Pedro y San Pablo', month=6, day=29, observance=strict_next_monday),
        Holiday('Asunción de la Virgen', month=8, day=15, observance=strict_next_monday),
        Holiday('Día de la raza', month=10, day=12, observance=strict_next_monday),
        Holiday('Todos los santos', month=11, day=1, observance=strict_next_monday),
        Holiday('Independencia de Cartagena', month=11, day=11, observance=strict_next_monday)
    ]
    
Colombian_BD = CustomBusinessDay(calendar=ColombianBusinessCalendar());


class DateUtils(object):

    date_init = None
    date_before = None
    dates = None
    holidays = None

    def __init__(self, date_init, date_before):
        self.date_init = date_init
        self.date_before = date_before
        self.dates = pd.DataFrame(
            {
                'date': pd.date_range(
                    self.date_init, self.date_before,
                    freq=Colombian_BD,
                    tz="America/Bogota"
                )
            }
        )
    def diff_days(self):
        return abs((self.date_before - self.date_init).days)
    
    def working_hours(self):
        print("dias trabajados L y V: ", len(self.dates))
        return (8 * len(self.dates))
    
    def diff_dates(self):
        diff_date = (self.date_before - self.date_init)
        days = self.diff_days()
        seconds = diff_date.seconds
        hours = int(seconds / 60 / 60)
        seconds -= hours * 60 * 60
        minutes = int(seconds/60)
        seconds -= minutes*60
        return f"{days} días, {hours} horas, {minutes} minutos y {seconds} segundos"
    
    def total_diff(self):
        diff_date = (self.date_before - self.date_init)
        days = self.diff_days()
        seconds = diff_date.seconds
        hours = days * 24 + (seconds / 60 / 60)
        days += ((seconds / 60 / 60) / 24)
        seconds += (hours * 60 * 60)
        return f"{seconds} segundos, {hours} horas, {days} dias"

        
    

# date example 15/06/2022 09:01:00
# date example 25/06/2022 15:30:00
format_date = "%d/%m/%Y %H:%M:%S%z"
A1 = "15/06/2022 09:01:00-04:00"
B1 = "10/07/2022 15:30:00+09:19"
print("date 1: ", A1)
print("date 2: ", B1)
#A1 = input(f"Ingrese la fecha A en formato DD/MM/YYYY hh:mm:ss ")
#B1 = input(f"Ingrese la fecha B en formato DD/MM/YYYY hh:mm:ss ")
# 'Etc/GMT-12' 'America/Bogota' 'Japan'
america_bogota_timezone = timezone('America/Bogota')
A = datetime.strptime(A1, format_date).replace(tzinfo=america_bogota_timezone)
B = datetime.strptime(B1, format_date).replace(tzinfo=america_bogota_timezone)


instance_date = DateUtils(A, B)
diff_days = instance_date.diff_days()
working_hours = instance_date.working_hours()
diff_dates = instance_date.diff_dates()
total_diff = instance_date.total_diff()
print(f"Diferencia en dias: {diff_days}")
print(f"Horas trabajadas:  {working_hours}")
print(diff_dates)
print(total_diff)
